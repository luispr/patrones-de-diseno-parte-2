package ar.uba.fi.tecnicas.exerecise1.capitalStrategy;

public class CapitalStrategyRevolver implements CapitalStrategy {

    @Override
    public double execute() {
        return 2;
    }
    
}
