package ar.uba.fi.tecnicas.exerecise1.capitalStrategy;

public class CapitalStrategyTermLoan implements CapitalStrategy {

    @Override
    public double execute() {
        return 1;
    }
    
}
