package ar.uba.fi.tecnicas.exerecise1;

import java.util.Date;

import ar.uba.fi.tecnicas.exerecise1.capitalStrategy.CapitalStrategy;
import ar.uba.fi.tecnicas.exerecise1.capitalStrategy.CapitalStrategyRCTL;
import ar.uba.fi.tecnicas.exerecise1.capitalStrategy.CapitalStrategyRevolver;
import ar.uba.fi.tecnicas.exerecise1.capitalStrategy.CapitalStrategyTermLoan;

public class Loan {
    private final double commmitment;
    private final double outstanding;
    private final int riskRating;
    private final Date maturity;
    private final Date expiry;
    private CapitalStrategy capitalStrategy;

    public Loan(final double commmitment, int riskRating, Date maturity) {
        this(commmitment, 0.00, riskRating, maturity, null);
    }

    public Loan(final double commmitment, int riskRating, Date maturity, Date expiry) {
        this(commmitment, 0.00, riskRating, maturity, expiry);
    }

    public Loan(final double commmitment, double outstanding, int riskRating, Date maturity, Date expiry) {
        this(null, commmitment, outstanding, riskRating, maturity, expiry);
    }

    public Loan(final CapitalStrategy capitalStrategy, final double commmitment, int riskRating, Date maturity, Date expiry) {
        this(capitalStrategy, commmitment, 0.00, riskRating, maturity, expiry);
    }

    public Loan(final CapitalStrategy capitalStrategy, final double commmitment, double outstanding, int riskRating, Date maturity, Date expiry) {
        this.commmitment = commmitment;
        this.outstanding = outstanding;
        this.riskRating = riskRating;
        this.maturity = maturity;
        this.expiry = expiry;
        this.capitalStrategy = capitalStrategy;

        if (capitalStrategy == null) {
            if (expiry == null) {
                this.capitalStrategy = new CapitalStrategyTermLoan();
            } else if (maturity == null) {
                this.capitalStrategy = new CapitalStrategyRevolver();
            } else {
                this.capitalStrategy = new CapitalStrategyRCTL();
            }
        }
    }

    public double getCommmitment() {
        return commmitment;
    }

    public double getOutstanding() {
        return outstanding;
    }

    public int getRiskRating() {
        return riskRating;
    }

    public Date getMaturity() {
        return maturity;
    }

    public Date getExpiry() {
        return expiry;
    }

    public CapitalStrategy getCapitalStrategy() {
        return capitalStrategy;
    }
}
