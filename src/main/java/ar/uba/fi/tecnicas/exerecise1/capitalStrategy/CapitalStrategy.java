package ar.uba.fi.tecnicas.exerecise1.capitalStrategy;

public interface CapitalStrategy {
    public double execute();
}
