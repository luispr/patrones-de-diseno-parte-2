package ar.uba.fi.tecnicas.exerecise1.capitalStrategy;

public class CapitalStrategyRCTL implements CapitalStrategy {

    @Override
    public double execute() {
        return 3;
    }
    
}
