package ar.uba.fi.tecnicas.exercise1;

import static org.junit.Assert.assertNotNull;
import java.util.Date;
import org.junit.Test;
import ar.uba.fi.tecnicas.exerecise1.Loan;

public class LoanTest {
    @Test
    public void testCreateTermLoan() {
        final double commmitment = 1;
        final int riskRating = 2;
        final Date maturity = new Date();

        Loan termLoan = new Loan(commmitment, riskRating, maturity);

        assertNotNull(termLoan);
    }
}
